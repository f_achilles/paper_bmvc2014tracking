\documentclass{bmvc2k}

\usepackage[utf8]{inputenc}
\graphicspath{{./images/}}
%\usepackage{caption}
\usepackage{subfigure}
\usepackage{booktabs}
\usepackage{placeins}
%% Enter your paper number here for the review copy
%\bmvcreviewcopy{???}

\title{Covariance-Based Fusion of Multi-Modal Tracking Data}

% Enter the paper's authors in order
\addauthor{Felix Achilles}{achilles@in.tum.de}{1}
\addauthor{Amit Shah}{shah@in.tum.de}{1}
\addauthor{Bernhard Fürst}{fuerst@in.tum.de}{1}
\addauthor{Nassir Navab}{nassir.navab@tum.de}{1}
%\addauthor{Susan Student}{http://www.vision.inst.ac.uk/~ss}{1}
%\addauthor{Petra Prof}{http://www.vision.inst.ac.uk/~pp}{1}
%\addauthor{Colin Collaborator}{colin@collaborators.com}{2}

% Enter the institutions
% \addinstitution{Name\\Address}
\addinstitution{
Chair for Computer Aided Medical Procedures\\
Technische Universität München\\
Munich, GER}
%\addinstitution{
% Collaborators, Inc.\\
% 123 Park Avenue,\\
% New York, USA
%}

\runninghead{Achilles et al.}{Covariance-Based Fusion of Tracking Data}

% Any macro definitions you would like to include
% These are not defined in the style file, because they don't begin
% with \bmva, so they might conflict with the user's own macros.
% The \bmvaOneDot macro adds a full stop unless there is one in the
% text already.
\def\eg{\emph{e.g}\bmvaOneDot}
\def\Eg{\emph{E.g}\bmvaOneDot}
\def\etal{\emph{et al}\bmvaOneDot}

%------------------------------------------------------------------------- 
% Document starts here
\begin{document}

\maketitle

\begin{abstract}
Tracking of surgical tools or hand-held medical imaging devices is a crucial%todoamit important component
element of computer assisted interventions. Previous attempts to improve tracking accuracy either use a single-model Kalman filter for noise reduction or an exclusive switching of tracking modalities in case the preferred tracking is lost.
In this work we propose a continuous fusion of all available tracking devices, based on the uncertainty of each tracking stream.
Before fusing the tracking information, each stream is filtered through a mixed-model Kalman filter in order to accommodate for static, continuous or accelerated motion.
This novel approach is validated through simulated and real tracking data and is shown to enhance accuracy and robustness against occlusion or distortion of the tracking stream.
\end{abstract}

%------------------------------------------------------------------------- 
\section{Introduction}
\label{sec:intro}

Modern computer assisted interventions often need tracking of surgical tools and hand-held medical imaging devices. The accurate and precise tracking can greatly enhance the outcome of surgical procedures.
In the regular use case of overlaying tracked tool positions on pre-operative anatomical information, significant improvements in the surgery outcome have been shown~\cite{Tingart2008,Mason2007,Fried1997}.
When tracked medical imaging devices are used for an intra-operative image reconstruction as in~\cite{Wendler2010,Feurer2012}, tracking accuracy directly affects the resulting image quality.
Therefore, tracking techniques that are robust to short occlusion or other distortions indeed help the physician and improve the acceptance of CAS systems.

Optical and electromagnetic (EM) tracking have become the commonly used modalities for tracking in the operating room.
Less frequently, mechanical and inertial sensors are employed, followed by experimental techniques such as Bragg fiber grating. The tracking technologies are desired to offer features such as uninterrupted availability, robustness to occlusion and distortion, six degrees of freedom information, high accuracy, precision, high data rate and a large operational volume. None of the current technologies can offer all of the desired properties in the OR. 
Hence, it was instead attempted to fuse the strengths of different modalities to achieve these goals.
Among the firsts to propose a combination of optical and electromagnetic tracking were Birkfellner~\etal\cite{Birkfellner1998}.
In their paper, a scheme for switching between modalities is investigated: when optical tracking information is unavailable due to loss of sight, electromagnetic measurements are polled in order not to lose the tracking.
The two trackers are mounted on the same instrument and calibrated beforehand.
Evaluation is performed for static pose measurements, resulting in an accuracy of $2.1\pm0.8\,$mm.
Nakamoto~\etal\cite{Nakamoto2002,Nakamoto2008} used a similar scheme and added the aspects of temporal calibration and speed dependency in their work.
Ren~\etal\cite{Ren2012} used an inertial measurement unit to perform a pose prediction.
The position prediction is then updated through the measurement of an EM tracking device in a Kalman filter.
Feuerstein~\etal\cite{Feuerstein2009a} combine EM and optical tracking to estimate the pose of a laparoscopic ultrasound transducer.
Additionally, the EM measurements from the tip of the device inside the patient's body are corrected through a mathematical bending model of the flexible laparoscope tip.
Vaccarella~\etal\cite{Vaccarella2013} improved Birkfellner's original scheme of using EM instead of optical measurements when the latter were not available.
They feed the optical pose information to a Kalman filter assuming constant velocity.
In case, optical tracking is lost or single optical markers are occluded, EM measurements served as input to that Kalman filter.

All of the presented fusion schemes suffer from some common drawbacks.
Most approaches exclusively used one tracking modality at a time, discarding the other available pose measurements.
Although Ren~\etal constantly used IMU and EM measurements, they do not employ a weighting of those two very different modalities.
If one tracking stream provides unreliable measurements (loss of an optical marker, magnetic disturbances in EM tracking, fast motion that corrupts the accelerometers inside the IMU) it should be regularized in the final pose estimation with a reduced weight.

Additionally, when a Kalman filter was used, its transition function to predict the next state ahead was always predefined and constant.
This can lead to high prediction errors as the tracked tool does not always follow commonly used constant velocity motion model.

Another shortcoming is the lack of a valid way to evaluate tracking performance during motion.
Evaluation of static pose measurement performance is established and provides information about how precise a certain point in the operating volume can be reached, see Maier-Hein~\etal\cite{MaierHein2012} or Wiles~\cite{Wiles2004}.
Specific use-cases as moving along a trajectory require high motion tracking accuracy.
Therefore, a way of measuring dynamic pose estimation performance of tracking devices and fusion schemes is required.

In this paper, we propose a set of new tracking fusion paradigms for the operating room.
Additionally, the goal of a dynamic tracking evaluation is approached in a new and easily applicable way, allowing to test performance of new tracking techniques not only at static positions, but also for the motion use-case.

\begin{figure}[htb]
\centering
\includegraphics[width=0.9\textwidth]{TheWholeThing.pdf}
\caption[Tracking fusion scheme]{Proposed tracking fusion scheme: EM and optical tracking on the left are taken as input to the fusion algorithm. Each modality is individually filtered through an Interacting Multiple Model (IMM) Kalman filter. Fusion of state vectors $x$ is then achieved based on their respective covariance $P$. The final ouput also includes the current estimated type of motion $\mu$ and possible warning flags in case one modality is lost (red dot) or exceeds an error threshold (yellow dot). Fused pose estimation can subsequently be streamed to various use-cases as suggested in~\cite{Schoch2013}.}
\label{fig:fullscheme}
\end{figure}

Figure~\ref{fig:fullscheme} provides a quick overview over the new tracking fusion scheme, exemplary showing one optical and one EM input device as well as possible use-cases for the fused tracking information.
Main contributions of this paper are:
\begin{itemize}
\item A new scheme of continuous fusion is proposed, where all available tracking modalities are combined based on their respective pose estimation covariance,
\item To account for rapidly changing types of motion, multiple-model Kalman filters are introduced,
\item Validation of the proposed technique is achieved for dynamic tool motion, providing a performance evaluation method for realistic use-cases.
\end{itemize}

%\begin{itemize}
%\item Professionals want their work cited
%\item start with tracking in general
%\item go to modalities? NO! this is not about the devices!
%\item then go to attempts of fusing information directly as in thesis: Birkfellner, Nakamoto, Feuerstein, Vaccarella
%\begin{itemize}
%\item Optical, Electromagnetic, Robotic and IMUs
%\item problem of dynamic validation
%\end{itemize}
%\item This is not about the devices, its about solving shortcomings through Fusion
%\begin{itemize}
%\item different input device frequencies, as in Vaccarella --> Kalman Prediction
%\item Occlusion
%\item Distortion
%\item Quantification of uncertainty
%\end{itemize}
%\item show image of Method (information graph with output)
%\item tracking post-processing should be ready for different use cases (motion, pointing, scanning)
%\item summarize Contributions
%\end{itemize}

\section{Methods}
\label{sec:methods}
For an accurate tracking fusion, all tracking devices needed to be co-registered in time (temporal calibration) and in space (hand-eye calibration).
Subsequently, each tracking stream was supplied to a multi-model Kalman filter.
Then, all pose estimates were mapped to a common location and fused based on their respective state covariance.
For validation through simulation, this fused pose was compared to the ground truth of simulated trajectories. For real-data validation, an ultrasound compounding of phantom scans was built using fused pose information and the result was compared to known actual phantom geometries.
\subsection{Calibration}
\paragraph{Temporal Calibration}
Tracking devices run at different sample rates and may be polled by the main thread at different times, so that their measurements need to be mapped to a common time frame.
For our real measurements, internal calculation times of optical and EM tracking devices were known.
Through central time-stamping in the main thread and subsequent subtraction of the time differences the common time frame could be established.
Temporal calibration of the tracking devices to the ultrasound machine was achieved through segmenting a line object in the ultrasound (US) image during a recording.
The extracted line motion was then compared to motion of the attached trackers and a time shift was calculated using the PLUS framework~\cite{Lasso2012d}.
\paragraph{Hand-Eye Calibration}
For simple US compounding with either EM or optical tracking, the PLUS framework also provided tracker-to-image spatial calibration.
For precisely registering the EM tracker to the optical tracker on the US transducer, a combined hand-eye calibration was used.
First, the known hand-eye equation $AX=XB$ was solved through the least-squares approach by Tsai~\etal\cite{Tsai1989}.
Secondly, if the residual error superseded  1\,mm, the non-linear optimization scheme by Zhao~\cite{Zhao2011} was applied.
\subsection{Filtering and Fusion}
\paragraph{Multi-Model Kalman Filter}
The original Kalman filter assumes a linear state model:
\begin{align}
\label{eqn:KalmanStateSpace}
		x_{k+1} &= Ax_k + Bu_k + v_k, \\
		z_{k+1} &= Hx_{k+1} + w_{k+1}~~,
\end{align}
where $x$ is the state vector, $u$ a known system input, $z$ the measurement vector, $A$ the transition matrix between two subsequent states, $B$ the input gain matrix, $H$ the measurement matrix that converts from state- to measurement-space. $w$ and $v$ are uncorrelated white Gaussian error variables and $k$ denotes the time index.
% TODO! change this a bit so it is not like in the masters thesis

Based on the state-space~\eqref{eqn:KalmanStateSpace} the filter estimates $x$ and a state covariance matrix $P$ in a two-step model.
Here, $P$ indicates how much trust can be given to the estimate at a certain time $k$.
First, a prediction step is executed in~\eqref{eqn:KalmanPred}, which is then followed by an update step that incorporates the current measurement~\eqref{eqn:KalmanUpdate}.
\begin{align}
\label{eqn:KalmanPred}
& \left.
\begin{aligned}
		 \hat{x}_{k}^- &= A\hat{x}_{k-1}, \\
		 P_{k}^- 		&= AP_{k-1}A^T+Q, 
\end{aligned}
 \right\rbrace
& \text{Prediction step} \\
\label{eqn:KalmanUpdate}
& \left.
\begin{aligned}
		K_{k} 		&= P_{k}^-H^T {\left( HP_{k}^-H^T + R \right)}^{-1}, \\
		 \hat{x}_{k}&= \hat{x}_{k}^- + K_{k}\left(z_{k}-H\hat{x}_{k}^-\right), \\
		 P_{k} 		&= \left(I-K_{k}H\right)P_{k}^-, 
\end{aligned}
 \right\rbrace
& \text{Update step}
\end{align}
where $Q$ is a state-noise and $R$ is a measurement-noise covariance matrix.
Kalman filter estimates only depend on the previous state and the current measurement.
Under the assumption of additive Gaussian noise and independent state vector elements it provides optimal estimates, see~\cite{Kalman1960}.
\begin{figure}[htb]
\centering
\includegraphics[width=0.9\textwidth]{JO_IMM_scheme.png}
\caption{HERE, WE PUT OR OWN SCHEME, NOT THIS ONE BUT SIMILAR}
\label{fig:IMMscheme}
\end{figure}
The transition matrix $A$ governs the way in which future states are predicted by the filter due to object motion.
Its function is therefore also referred to as motion model.
In order to accommodate for the cases of static pose, constant velocity motion and accelerated motion, three different motion models were combined in an Interacting Multiple Model scheme~\cite{Blom1988}:
As shown in figure~\ref{fig:IMMscheme}, the filtering process begins from left to right with the interaction or 'mixing' process:
\begin{equation}
\begin{aligned}
\mu_{k-1|k-1}^i &= \sum_j H_{ij}\mu_{k-1}^j,\\
x_{k-1|k-1}^i 	&= \sum_j H_{ij}\mu_{k-1}^j x_{k-1}^j / \mu_{k-1|k-1}^i,\\
P_{k-1|k-1}^i 	&= \sum_j H_{ij}\mu_{k-1}^j \left[ P_{k-1}^j + (x_{k-1}^j - x_{k-1|k-1}^i)(x_{k-1}^j - x_{k-1|k-1}^i)^T \right] / \mu_{k-1|k-1}^i ~,
\end{aligned}
\end{equation}
where $x,P,\mu,H_{ij}$ are state vector, covariance matrix, model probability and switching matrix respectively.
The switching matrix $H_{ij}$ describes a Markov chain that describes how probable it is to switch from model $j$ to model $i$ in each of its elements $h_{ij}$.
The updated model states are then propagated through the different Kalman filters in order to incorporate the new measurement.
Additionally, the filters need to output a certain state of the covariance matrix, referred to as $S$, which is built from
\begin{equation}
S = HP^{-}H^T + R,
\end{equation}
where $H$ is the measurement matrix, $P^-$ the covariance matrix after the prediction step~\eqref{eqn:KalmanPred} and $R$ the measurement noise covariance matrix.
From that matrix $S$, the a posteriori model probabilities $\mu^i$ are computed as follows:
\begin{equation}
\begin{aligned}
\mu^i_{k} =~&c \cdot \mu^i_{k-1|k-1}	\cdot exp\left( -1/2\cdot{\nu^i_k}^T {S^i_k}^{-1} \nu^i_k\right) / \sqrt{||S^i_k||}, \\
&c ~ \mathrm{~is~chosen~so~that\-:} \sum_i \mu^i_{k} = 1 ~~,
\end{aligned}
\end{equation}
where $\nu$ is the residual of the respective Kalman filter, i.e.\ $\nu=z-Hx^-$.
The new combined state $\hat{x}$ and its covariance $\hat{P}$ can be built from each of the models' outputs and the calculated a posteriori model probabilities $\mu^i_{k}$:
\begin{equation}
\begin{aligned}
\hat{x}_k &= \sum_i \mu^i_{k} x^i_{k,k},\\
\hat{P}_k &= \sum_i \mu^i_{k} \left[ P^i_{k,k} + (x^i_{k,k} - \hat{x}_k)(x^i_{k,k} - \hat{x}_k)^T\right] ~~.
\end{aligned}
\end{equation}
\paragraph{Covariance-Based Fusion}
The presented IMM combination of three motion models is performed for each tracking input stream.
After each modality $m$ is provided with a pose estimate $\hat{x}_k^m$ at the time $k$, those estimates are mapped to a common location e.g.\ one of the trackers on the tool.
If all measurements were perfect and synchronized, the mapped estimates would be the same.
However each modality's estimate has its own uncertainty, expressed through its covariance matrix  $\hat{P}_k^m$.
The optimal way to fuse those pose estimates is to weigh them with the inverse of their respective covariance matrix, as in~\cite{Lee2003}:
\begin{equation}
\bar{x}_k=\frac{\sum_m {\left[P^m_k\right]}^{-1}\cdot x^m_k}{\sum_m {\left[P^m_k\right]}^{-1}}~~.
\label{eqn:MasterFilter}
\end{equation}
%This scheme of combining federated IMM Kalman filter estimates can also be called a 'master filter' that fuses many local filter estimates.
It can be seen from equation~\eqref{eqn:MasterFilter}, that pose estimates with a high covariance, i.e.\ uncertainty, are not affecting the end result as much as estimates with a low covariance, which is just the desired effect for a useful sensor fusion.
\subsection{Generation of Artificial Trajectories}
To not be dependent on external error factors such as imprecise calibration (temporal and spatial) and unreliable ground truths, tool trajectories were simulated to validate the proposed fusion method.
A line, a circle and a curved eight-shaped trajectory were created.
Those artificial trajectories were fed into the fusion algorithm in order to determine the effects of additive measurement noise, tracking sample rate and speed of motion.
Furthermore, the IMM approach enabling an automated combination of three motion models was evaluated against each motion model (static, constant velocity and constant acceleration) on its own.
To test for all possible influences, distortion of the artificial EM tracking as well as occlusion of the optical tracking were simulated.
A state fusion with equal weights was compared to the proposed one using covariance-based weights.
% eval static position with velocity Kalman filter
\subsection{Validation through Ultrasound-Compounding}
An ultrasound transducer was optically tracked with an NDI Polaris system at 20\,Hz and electromagnetically tracked with an NDI Aurora Tabletop system at 40\,Hz.
In order to minimize hand-eye calibration error, tracking markers on the US transducer were placed close to each other.
Metallic objects were kept at distance to not interfere with EM tracking.
For validation, freehand sweep scans were acquired for two different wire phantoms and one prostate phantom, see Fig.~\ref{fig:phantoms}.
Three-dimensional volumes were calculated form the scans through ultrasound compounding.
An error measure was defined as the RMS error between computed and known phantom geometry.
Each phantom was scanned 10 times in order to calculate the average error and its standard deviation.

\section{Results}
\subsection{Simulated Trajectories}
\begin{figure}
\centering
\subfigure[line data]{
\includegraphics[width=0.3\textwidth]{artData_Line}
%\caption{line data}
}
~
\subfigure[circle data]{
\includegraphics[width=0.3\textwidth]{artData_Circle}
%\caption{circle data}
}
~
\subfigure[eight data]{
\includegraphics[width=0.3\textwidth]{artData_Eight}
%\caption{eight data}
}
\caption[Artificial data trajectories]{Artificial data trajectories: $x$ marks optical tracker positions, $+$ marks electromagnetic tracker positions, $o$ marks combined position estimates,  red/green arrows depict orientation of the optical/electromagnetic tracker, respectively; (a) Line trajectory with constant speed and constant orientation, (b) Circle trajectory with constant speed but varying orientation, (c) Eight-shaped trajectory with rapidly changing orientation and varying translational speed.}
\label{fig:artTrajectories}
\end{figure}

\begin{table}[width=\textwidth]
\centering
\caption{Results of 3 artificial trajectories filtered with the 'constant position' Kalman Filter at different speeds. Sample frequencies of both simulated devices were set to 20\,Hz.}
\label{tab:constPosResults}
\input{./tables/ResultsConstPos20Hz.tex}
\end{table}

\begin{table}[width=\textwidth]
\centering
\caption{Results of 3 artificial trajectories filtered with the 'constant velocity' Kalman Filter at different speeds. Sample frequencies of both simulated devices were set to 20\,Hz.}
\label{tab:constVelResults}
\input{./tables/ResultsConstVel20Hz.tex}
\end{table}

\begin{table}[width=\textwidth]
\centering
\caption{Results of 3 artificial trajectories filtered with the 'constant acceleration' Kalman Filter at different speeds. Sample frequencies of both simulated devices were set to 20\,Hz.}
\label{tab:constAccResults}
\input{./tables/ResultsConstAcc20Hz.tex}
\end{table}

\FloatBarrier

\subsection{Real Tracking Data}
%\subsubsection{Calibration Performance}
\subsubsection{Phantom Results}

\section{Conclusion}
\bibliography{ThesisLibrary}
\end{document}
